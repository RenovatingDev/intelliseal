# Sealify: Kubeseal Helper for IntelliJ

This Plugin provides a way to seal values in YAML-Files via [kubeseal](https://github.com/bitnami-labs/sealed-secrets)
from within IntelliJ.
This is done by providing an "Intention", which can be triggered by pressing `alt` + `enter` on any key in a YAML file:

```yaml
kind: SealedSecret
metadata:
  creationTimestamp: null
  name: secret-name
  namespace: default
spec:
  encryptedData:
    # name: k8s-secret-name
    # namespace: k8s-namespace-name
    foo: bar
    key: value
```

By placing the cursor on `foo` or `bar` and pressing `alt` + `enter`, you can easily transform this into a sealed
secret:

```yaml
kind: SealedSecret
metadata:
  creationTimestamp: null
  name: secret-name
  namespace: default
spec:
  encryptedData:
    # name: k8s-secret-name
    # namespace: k8s-namespace-name
    foo: AgBXyeiCcNY8MIVD8/4doluT1DjjM9sCo5ZlNhtKaOf1oWsaVhEoWnL/LAKvAfGAg5M91H24WTpbtoQ8lM2qBduAnMl6pIkbxILk0WtOpdh56nOYIN8RnIfhSF2F1wlMbSAgnzSs8DlsNjAyT9Jq95pz9C9n/Cigaam7eCrHL+XfHd/XjXZ5qavg13ATWQ7PZZPcqxioNoE9aUNlHIdLUdcrnwLrWfRlgDOcXyiLc5SZ74M3NfEjsIxvxn2My4g2fbhJRGrO2cKKEj/E1FHqlN8p5O0QqnfEqaWbnc6pkQC46f/RIKEyuHbYLt1UWPEzHkBYtwSL7uhYxLHjH4WLMJeaC3xQTqC1b9/Fl3nKcA94eLyQBIo3Ltnln05WShJ6u48xQ4jMAljaTl9WOBlxI9uYIEm+1hdU4j0I2L7402lekkzaezu+4dQ1WLeBfuU9kWItesPMjJzj+/EuW9BxAgZ2U8jLZa2WLNzuicpFgaQBQe1La4WEWw4VWUqYSlrz2RNGnZJUdWvf//FZTLtlISPN2HcIbtfBkolLBi+DHADkHZIlLGc3lrhm4zRQuiYy28pYmPOkT5Sokx1b+HB0DPtYVtJg/DAZwehAcWHtZraOJZiv+wPI/HBMmkxGMHl7gGfsDeNfCvL9wusreA8G6a+8C1M5f8UojRWIofiufcN36JjVH3CGwrzNgB8Kyp2nR7r/9rw=
    key: AgCoOJUIWZT4pdXLRYJbJyF4ILZF7T0P1TWHXsDRWbwBqwR8lLffS0A5f9qO7lzpgUPYCpDvIZz/Buwt0pVl0YUH4kHHkN0rsISxrpHBACqJMn3zjcPpRWsAYd2gWs8GM4Unqg8RoyRPyHbpGD/fQdB7XCzXzYE+VBPcDwmLUX0N/+mcvgvsNbNalZ5haYXWeSmrzYFBM/z8l2fPTJ7pBqZscp2ClyzSn4pftekmuKjEcvqT8ojNE+RkMhiol0wm9E43MG8Hhc2E0hxEmRdkEot88+EdJ3r4E+FVZP5WcxhT9jhCW7m5FkElmJ2Zfi5VEZo5qKkvPrirbZPw2XZPiL6+GH5arh42eVvaJeJ+Gig80ngSYOjQSkhb7Z62iAFCjKOvoVNoxgdq3PXoFj8X+0W83hpKiT9+xX0Y1aWDIrC2DD/5Kv1+2q1AJDw19DB7shEe+I0xu7lXCk/54JURLhBy8heplNrtp+aQxh08aAMIKdjlSnabWOF8Xn7k4XRagf+tDv0Bc82dgohEqQ7Cbp2rF67RzMsCsS1ZDORjsiR9kVE3ZJmir9mrERQEJ6B5oe5kuP+XIRPVms3LVYiB09WnTJXtLICsVJGH5ad3xVOO4XDRf9zeb3daOxNIVfL8rUM2VFi34lItZWbmULcRe2xK8NKCM5ezrVXP2BEAqyHowzzH+qaSkFVdGFNYwLvyuVIffaC19A==
```

The comment above foo allows to specify the name and namespace used for sealing the secret.
If they are missing, the plugin will ask for them.

# This Is an Early Alpha Version of This Plugin

Many features are still missing and there may be bugs.

## Planned, but Missing Features

- Support for `namespace-wide` and `cluster-wide` scopes
- Seal multiple values at once
- Seal selections
- Option to generate the comments with name & namespace automatically
- Support for JSON
- ...?