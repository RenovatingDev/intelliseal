package de.jonaseckstein.kubeseal.intentions

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.codeInsight.intention.PriorityAction
import com.intellij.codeInsight.intention.PsiElementBaseIntentionAction
import com.intellij.codeInsight.intention.preview.IntentionPreviewInfo
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.options.ShowSettingsUtil
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.util.NlsContexts.PopupTitle
import com.intellij.psi.PsiComment
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.childrenOfType
import com.intellij.psi.util.parentOfType
import com.intellij.ui.SimpleListCellRenderer
import de.jonaseckstein.kubeseal.REGEX_RFC_1123_LABEL_PATTERN
import de.jonaseckstein.kubeseal.SealDialog
import de.jonaseckstein.kubeseal.settings.Configuration
import de.jonaseckstein.kubeseal.settings.ConfigurationState
import de.jonaseckstein.kubeseal.settings.model.SealingKey
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLMapping
import org.jetbrains.yaml.psi.YAMLScalar
import java.nio.file.Files
import java.nio.file.Path


open class KubesealIntention : AbstractKubesealIntentionBase() {
    private val LOG = logger<KubesealIntention>()
    override fun getText(): String {
        return "Seal value with ..."
    }

    override fun getPriority(): PriorityAction.Priority {
        return PriorityAction.Priority.LOW
    }

    override fun isAvailable(project: Project, editor: Editor?, element: PsiElement): Boolean {
        if (element.language.id != "yaml") {
            return false
        }

        val ctx = element.context
        if (ctx !is YAMLKeyValue) {
            return false
        }

        return ctx.value is YAMLScalar
    }

    override fun invoke(project: Project, editor: Editor?, element: PsiElement) {
        val yamlKeyValue = element.context as? YAMLKeyValue ?: return

        if (ConfigurationState.getInstance().sealingKeys.isEmpty()) {
            showInvalidSettingsPopup("No Sealing Keys Configured", project, editor!!)
            return
        }
        val popup = JBPopupFactory.getInstance().createPopupChooserBuilder(
            ConfigurationState.getInstance().sealingKeys
        ).setTitle("Select Key to Use")
            .setRenderer(SimpleListCellRenderer.create("") { it.alias })
            .setItemChosenCallback {
                WriteCommandAction.runWriteCommandAction(project) {
                    ConfigurationState.getInstance().lastUsedKeyIdx =
                        ConfigurationState.getInstance().sealingKeys.indexOf(it)
                    checkRequirementsAndSeal(project, editor!!, yamlKeyValue, it)
                }
            }
            .createPopup()
        popup.showInBestPositionFor(editor!!)
    }
}