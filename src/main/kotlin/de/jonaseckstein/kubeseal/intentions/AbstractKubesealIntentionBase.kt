package de.jonaseckstein.kubeseal.intentions

import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.codeInsight.intention.PriorityAction
import com.intellij.codeInsight.intention.PsiElementBaseIntentionAction
import com.intellij.codeInsight.intention.preview.IntentionPreviewInfo
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.options.ShowSettingsUtil
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.util.NlsContexts
import com.intellij.psi.PsiComment
import com.intellij.psi.PsiFile
import com.intellij.psi.util.childrenOfType
import com.intellij.psi.util.parentOfType
import de.jonaseckstein.kubeseal.REGEX_RFC_1123_LABEL_PATTERN
import de.jonaseckstein.kubeseal.SealDialog
import de.jonaseckstein.kubeseal.settings.Configuration
import de.jonaseckstein.kubeseal.settings.ConfigurationState
import de.jonaseckstein.kubeseal.settings.model.SealingKey
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLMapping
import org.jetbrains.yaml.psi.YAMLScalar
import java.nio.file.Files
import java.nio.file.Path
import java.util.function.Consumer

abstract class AbstractKubesealIntentionBase: PsiElementBaseIntentionAction(), IntentionAction, PriorityAction {
    private val LOG = logger<AbstractKubesealIntentionBase>()

    override fun getFamilyName(): String {
        return "Kubeseal"
    }

    override fun startInWriteAction(): Boolean {
        return false;
    }

    override fun generatePreview(project: Project, editor: Editor, file: PsiFile): IntentionPreviewInfo {
        val caretModel = editor.caretModel
        val position = caretModel.offset
        val element = file.findElementAt(position)
        val yamlKeyValue = element?.context as? YAMLKeyValue ?: return IntentionPreviewInfo.EMPTY

         forAllScalars(yamlKeyValue) {
            it.updateText("<encrypted value>")
        }
        return IntentionPreviewInfo.DIFF;
    }

    protected fun checkRequirementsAndSeal(project: Project, editor: Editor, elementToSeal: YAMLKeyValue, sealingKey: SealingKey) {
        val executable = ConfigurationState.getInstance().kubesealBinaryPath
        val cert = sealingKey.path
        if (!checkSettings(executable, project, editor, cert)) return
        val (name: String?, namespace: String?) = searchKubesealSettingsInComments(elementToSeal)

        if (name == null || namespace == null) {
            SealDialog(
                project, false, DialogWrapper.IdeModalityType.MODELESS
            ) { namespace0, name0 ->
                sealElement(executable, cert, namespace0, name0, elementToSeal, project, editor)
            }.show()
        } else {
            sealElement(executable, cert, namespace, name, elementToSeal,project, editor)
        }
    }

    private fun sealElement(
        executable: String,
        cert: String,
        namespace: String,
        name: String,
        elementToSeal: YAMLKeyValue,
        project: Project,
        editor: Editor
    ) {
        forAllScalars(elementToSeal) {
            // TODO: these are treated as individual write actions, therefore CTRL+Z reverts one of them
            //  it would be better to accumulate them and run them via `runWriteCommandAction` at once at the end
            //  this would also probably allow us to remove project and editor as paramaters in a few methods
            sealScalar(executable, cert, namespace, name, it, project, editor)
        }
    }

    protected fun forAllScalars(
        start: YAMLKeyValue,
        func: Consumer<YAMLScalar>
    ) {
        if(start.value is YAMLScalar) {
            func.accept(start.value as YAMLScalar)
        } else if(start.value is YAMLMapping) {
            val children = (start.value as YAMLMapping).childrenOfType<YAMLKeyValue>()
            for (child in children) {
                forAllScalars(child, func)
            }
        }
    }

    private fun sealScalar(
        executable: String,
        cert: String,
        namespace: String,
        name: String,
        scalarToReplace: YAMLScalar,
        project: Project,
        editor: Editor
    ) {
        val encrypted = de.jonaseckstein.kubeseal.encrypt(executable, cert, namespace, name, scalarToReplace.textValue)
        WriteCommandAction.runWriteCommandAction(project, "Seal Value","gid", {
            if (encrypted.isBlank()) {
                showInvalidSettingsPopup("Kubeseal or Cert Path Might Be Wrong", project, editor)
                return@runWriteCommandAction
            }
            scalarToReplace.updateText(encrypted)
        }, scalarToReplace.containingFile);
    }

    private fun searchKubesealSettingsInComments(scalarToReplace: YAMLKeyValue): Pair<String?, String?> {
        var name: String? = null
        var namespace: String? = null
        var parent = scalarToReplace.parentOfType<YAMLMapping>(false)
        while (parent != null) {
            val comments = parent.childrenOfType<PsiComment>()
            for (comment in comments) {
                val matches =
                    Regex("# *(name(?:space)?): *($REGEX_RFC_1123_LABEL_PATTERN)").matchEntire(comment.text)
                if (matches != null) {
                    val key = matches.groupValues[1]
                    val value = matches.groupValues[2]
                    when (key) {
                        "name" -> name = name ?: value
                        "namespace" -> namespace = namespace ?: value
                    }
                }
            }
            parent = parent.parentOfType<YAMLMapping>(false)
        }
        return Pair(name, namespace)
    }

    private fun checkSettings(
        executable: String,
        project: Project,
        editor: Editor,
        cert: String
    ): Boolean {
        // isExecutable has similar semantics as Unix x-flag. Directories can therefore be executable!
        if (Files.isDirectory(Path.of(executable)) || !Files.isExecutable(Path.of(executable))) {
            showInvalidSettingsPopup("Kubeseal Path Not Valid", project, editor)
            return false
        }
        if (!Files.isRegularFile(Path.of(cert))) {
            showInvalidSettingsPopup("Cert Path Not Valid", project, editor)
            return false
        }
        return true
    }

    protected fun showInvalidSettingsPopup(
        @NlsContexts.PopupTitle message: String,
        project: Project, editor: Editor
    ) {
        LOG.warn("$message, cant seal value")
        JBPopupFactory.getInstance().createConfirmation(
            "$message. Open Settings?", "Yes", "No",
            {
                // TODO: causes  "Slow operations are prohibited on EDT", but has to be run on EDT. -> Bug in IJ?
                ShowSettingsUtil.getInstance().showSettingsDialog(project, Configuration::class.java)
            }, 0
        ).showInBestPositionFor(editor)
    }
}
