package de.jonaseckstein.kubeseal.intentions

import com.intellij.codeInsight.intention.PriorityAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import de.jonaseckstein.kubeseal.settings.ConfigurationState
import de.jonaseckstein.kubeseal.settings.model.SealingKey
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLScalar

class KubesealLastUsedKeyIntention : KubesealIntention() {
    override fun getText(): String {
        return "Seal value with ${getSealingKey().alias}"
    }

    override fun getPriority(): PriorityAction.Priority {
        return PriorityAction.Priority.NORMAL
    }

    override fun isAvailable(project: Project, editor: Editor?, element: PsiElement): Boolean {
        if (ConfigurationState.getInstance().sealingKeys.size <= ConfigurationState.getInstance().lastUsedKeyIdx) {
            return false
        }

        return super.isAvailable(project, editor, element)
    }

    private fun getSealingKey(): SealingKey {
        val lastUsedKeyIdx = ConfigurationState.getInstance().lastUsedKeyIdx
        return ConfigurationState.getInstance().sealingKeys[lastUsedKeyIdx]
    }

    override fun invoke(project: Project, editor: Editor?, element: PsiElement) {
        val yamlKeyValue = element.context as? YAMLKeyValue ?: return
        checkRequirementsAndSeal(project, editor!!, yamlKeyValue, getSealingKey())
    }
}