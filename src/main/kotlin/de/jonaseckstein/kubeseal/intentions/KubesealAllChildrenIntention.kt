package de.jonaseckstein.kubeseal.intentions

import com.intellij.codeInsight.intention.PriorityAction
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.psi.PsiElement
import com.intellij.ui.SimpleListCellRenderer
import de.jonaseckstein.kubeseal.settings.ConfigurationState
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLMapping

open class KubesealAllChildrenIntention  : AbstractKubesealIntentionBase() {
    private val LOG = logger<KubesealAllChildrenIntention>()
    override fun getText(): String {
        return "Seal all children with ..."
    }

    override fun getPriority(): PriorityAction.Priority {
        return PriorityAction.Priority.LOW
    }

    override fun isAvailable(project: Project, editor: Editor?, element: PsiElement): Boolean {
        if (element.language.id != "yaml") {
            return false
        }

        val ctx = element.context
        if (ctx !is YAMLKeyValue) {
            return false
        }

        return ctx.value is YAMLMapping
    }

    override fun invoke(project: Project, editor: Editor?, element: PsiElement) {
        val yamlKeyValue = element.context as? YAMLKeyValue ?: return

        if (ConfigurationState.getInstance().sealingKeys.isEmpty()) {
            showInvalidSettingsPopup("No Sealing Keys Configured", project, editor!!)
            return
        }
        val popup = JBPopupFactory.getInstance().createPopupChooserBuilder(
            ConfigurationState.getInstance().sealingKeys
        ).setTitle("Select Key to Use")
            .setRenderer(SimpleListCellRenderer.create("") { it.alias })
            .setItemChosenCallback {
                WriteCommandAction.runWriteCommandAction(project) {
                    ConfigurationState.getInstance().lastUsedKeyIdx =
                        ConfigurationState.getInstance().sealingKeys.indexOf(it)
                    checkRequirementsAndSeal(project, editor!!, yamlKeyValue, it)
                }
            }
            .createPopup()
        popup.showInBestPositionFor(editor!!)
    }
}