package de.jonaseckstein.kubeseal.intentions

import com.intellij.codeInsight.intention.PriorityAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import de.jonaseckstein.kubeseal.settings.ConfigurationState
import de.jonaseckstein.kubeseal.settings.model.SealingKey
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLMapping

// TODO: refactor to remove more code duplication between all 4 intentions
class KubesealAllChildrenLastUsedKeyIntention : KubesealAllChildrenIntention() {
    override fun getText(): String {
        return "Seal all children with ${getSealingKey().alias}"
    }

    override fun getPriority(): PriorityAction.Priority {
        return PriorityAction.Priority.NORMAL
    }

    override fun isAvailable(project: Project, editor: Editor?, element: PsiElement): Boolean {
        if (ConfigurationState.getInstance().sealingKeys.size <= ConfigurationState.getInstance().lastUsedKeyIdx) {
            return false
        }

        if (element.language.id != "yaml") {
            return false
        }

        val ctx = element.context
        if (ctx !is YAMLKeyValue) {
            return false
        }

        return ctx.value is YAMLMapping
    }

    private fun getSealingKey(): SealingKey {
        val lastUsedKeyIdx = ConfigurationState.getInstance().lastUsedKeyIdx
        return ConfigurationState.getInstance().sealingKeys[lastUsedKeyIdx]
    }

    override fun invoke(project: Project, editor: Editor?, element: PsiElement) {
        val yamlKeyValue = element.context as? YAMLKeyValue ?: return
        checkRequirementsAndSeal(project, editor!!, yamlKeyValue, getSealingKey())
    }
}