package de.jonaseckstein.kubeseal

import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.components.JBLabel
import com.intellij.util.ui.FormBuilder
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JTextField

class SealDialog(
    project: Project?,
    canBeParent: Boolean,
    ideModalityType: IdeModalityType,
    onOk: (namespace: String, name: String) -> Unit
) :
    DialogWrapper(project, canBeParent, ideModalityType) {

    private val onOk: (namespace: String, name: String) -> Unit = onOk
    private val mainPanel: JPanel
    private val namespaceField = JTextField()
    private val nameField = JTextField()

    init {
        title = "Seal Value"
        mainPanel = FormBuilder.createFormBuilder()
            // TODO modes (strict, namespace restricted, open)
            .addLabeledComponent(JBLabel("Namespace: "), namespaceField, 1, false)
            .addLabeledComponent(JBLabel("Name: "), nameField, 1, false)
            .addComponentFillVertically(JPanel(), 0)
            .panel
        init()
    }

    override fun doValidate(): ValidationInfo? {
        // TODO report a list of ValidationInfo
        if (namespaceField.text.isBlank()) {
            return ValidationInfo("Namespace cant be empty", namespaceField)
        }
        if (nameField.text.isBlank()) {
            return ValidationInfo("Name cant be empty", namespaceField)
        }

        return null
    }

    override fun getPreferredFocusedComponent(): JComponent {
        return namespaceField
    }

    override fun createCenterPanel(): JComponent {
        return mainPanel;
    }

    override fun doOKAction() {
        super.doOKAction()
        onOk(namespaceField.text, nameField.text)
    }
}