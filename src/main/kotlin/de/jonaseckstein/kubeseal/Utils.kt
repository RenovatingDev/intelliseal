package de.jonaseckstein.kubeseal

import com.intellij.openapi.options.ShowSettingsUtil
import com.intellij.openapi.ui.popup.JBPopupFactory
import de.jonaseckstein.kubeseal.settings.Configuration

const val REGEX_RFC_1123_LABEL_PATTERN = "[a-z0-9]([a-z0-9-]*?[a-z0-9])?"
val REGEX_RFC_1123_LABEL = Regex(REGEX_RFC_1123_LABEL_PATTERN)

fun isRfc1123Label(s: String): Boolean {
    return s.matches(REGEX_RFC_1123_LABEL)
}
