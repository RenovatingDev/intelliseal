package de.jonaseckstein.kubeseal.settings

import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.TextBrowseFolderListener
import com.intellij.openapi.ui.TextFieldWithBrowseButton
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.ui.components.JBLabel
import com.intellij.util.Function
import com.intellij.util.ui.ColumnInfo
import com.intellij.util.ui.FormBuilder
import com.intellij.util.ui.table.TableModelEditor
import com.intellij.util.ui.table.TableModelEditor.DialogItemEditor
import de.jonaseckstein.kubeseal.settings.model.SealingKey
import org.apache.commons.lang3.StringUtils
import java.nio.file.Files
import java.nio.file.Path
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JTextField

// ColumnInfo is also available as editable in e.g. TableModelEditor.EditableColumnInfo
private val COLUMNS = arrayOf(object : ColumnInfo<SealingKey, String>("Alias") {
    override fun valueOf(item: SealingKey) = item.alias
}, object : ColumnInfo<SealingKey, String>("Path to Key") {
    override fun valueOf(item: SealingKey) = item.path
})

private val itemEditor = object : DialogItemEditor<SealingKey> {
    override fun getItemClass(): Class<out SealingKey> {
        return SealingKey::class.java
    }

    override fun applyEdited(oldItem: SealingKey, newItem: SealingKey) {
        oldItem.alias = newItem.alias
        oldItem.path = newItem.path
    }

    override fun edit(item: SealingKey, mutator: Function<in SealingKey, out SealingKey>, isAdd: Boolean) {
        SealingKeyEditDialog(item) { alias, path ->
            val editable = mutator.`fun`(item)
            editable.alias = alias;
            editable.path = path;
        }.show()
    }

    override fun clone(item: SealingKey, forInPlaceEditing: Boolean): SealingKey {
        return item.copy()
    }

    override fun isEmpty(item: SealingKey): Boolean {
        return StringUtils.isBlank(item.alias) && StringUtils.isBlank(item.path)
    }

    override fun isUseDialogToAdd(): Boolean {
        return true
    }
}

class SealingKeyEditDialog(
    sealingKey: SealingKey,
    private val onOk: (namespace: String, name: String) -> Unit
) :
    DialogWrapper(null, false, IdeModalityType.IDE) {
    private val mainPanel: JPanel
    private val aliasField = JTextField()
    private val pathField = TextFieldWithBrowseButton()

    init {
        title = "Edit Sealing Key"
        aliasField.text = sealingKey.alias;
        pathField.text = sealingKey.path;
        pathField.addBrowseFolderListener(
            TextBrowseFolderListener(
                FileChooserDescriptorFactory.createSingleFileDescriptor()
            )
        )
        mainPanel = FormBuilder.createFormBuilder()
            .addLabeledComponent(JBLabel("Alias: "), aliasField, 1, false)
            .addLabeledComponent(JBLabel("Path: "), pathField, 1, false)
            .addComponentFillVertically(JPanel(), 0)
            .panel
        init()
    }

    override fun createCenterPanel(): JComponent {
        return mainPanel;
    }

    override fun doValidate(): ValidationInfo? {
        // TODO report a list of ValidationInfo
        if (aliasField.text.isBlank()) {
            return ValidationInfo("Alias cant be empty", aliasField)
        }
        if (pathField.text.isBlank()) {
            return ValidationInfo("Path cant be empty", pathField)
        }
        if (!Files.isRegularFile(Path.of(pathField.text))) {
            return ValidationInfo("Path must point to file", pathField)
        }

        return null
    }

    override fun doOKAction() {
        super.doOKAction()
        onOk(aliasField.text, pathField.text)
    }
}

class SealingKeyEditor :
    TableModelEditor<SealingKey>(COLUMNS, itemEditor, "No keys defined") {
}
