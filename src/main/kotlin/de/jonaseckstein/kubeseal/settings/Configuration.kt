package de.jonaseckstein.kubeseal.settings

import com.intellij.openapi.options.Configurable
import javax.swing.JComponent

class Configuration : Configurable {
    private var configComponent = ConfigurationComponent()

    override fun createComponent(): JComponent {
        return configComponent.myMainPanel
    }

    override fun isModified(): Boolean {
        val settings = ConfigurationState.getInstance()
        var modified = settings.kubesealBinaryPath != configComponent.kubesealBinaryPath
        modified = modified || settings.sealingKeys.size != configComponent.sealingKeys.size
        for ((index, sealingKey) in settings.sealingKeys.withIndex()) {
            modified = modified || sealingKey != configComponent.sealingKeys[index]
        }
        return modified
    }

    override fun apply() {
        val settings = ConfigurationState.getInstance()
        settings.kubesealBinaryPath = configComponent.kubesealBinaryPath
        settings.sealingKeys = configComponent.sealingKeys
    }

    override fun getDisplayName(): String {
        return "Sealify Plugin"
    }

    override fun reset() {
        val settings: ConfigurationState = ConfigurationState.getInstance()
        configComponent.kubesealBinaryPath = settings.kubesealBinaryPath
        configComponent.sealingKeys = settings.sealingKeys
    }
}