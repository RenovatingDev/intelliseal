package de.jonaseckstein.kubeseal.settings.model

import com.intellij.util.xmlb.annotations.Attribute
import kotlinx.serialization.Serializable

@Serializable
data class SealingKey(
    @Attribute var alias: String = "",
    @Attribute var path: String = ""
)