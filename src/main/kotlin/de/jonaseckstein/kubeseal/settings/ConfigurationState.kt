package de.jonaseckstein.kubeseal.settings

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.util.xmlb.XmlSerializerUtil
import com.intellij.util.xmlb.annotations.XCollection
import de.jonaseckstein.kubeseal.settings.model.SealingKey

@State(
    name = "de.jonaseckstein.kubeseal.ConfigurationState",
    storages = [Storage("SealifyPlugin.xml")]
)
class ConfigurationState : PersistentStateComponent<ConfigurationState> {
    var kubesealBinaryPath: String = ""

    @XCollection
    var sealingKeys: List<SealingKey> = ArrayList()
    var lastUsedKeyIdx = 0

    companion object {
        public fun getInstance(): ConfigurationState {
            return ApplicationManager.getApplication().getService(ConfigurationState::class.java)
        }
    }

    override fun getState(): ConfigurationState {
        return this
    }

    override fun loadState(state: ConfigurationState) {
        XmlSerializerUtil.copyBean(state, this)
    }
}