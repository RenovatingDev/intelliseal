package de.jonaseckstein.kubeseal.settings

import com.intellij.openapi.fileChooser.FileChooserDescriptor
import com.intellij.openapi.ui.TextBrowseFolderListener
import com.intellij.openapi.ui.TextFieldWithBrowseButton
import com.intellij.ui.components.JBLabel
import com.intellij.util.ui.FormBuilder
import de.jonaseckstein.kubeseal.settings.model.SealingKey
import javax.swing.JPanel


class ConfigurationComponent() {
    var myMainPanel: JPanel
        private set;
    private val kubesealBinaryPathField = TextFieldWithBrowseButton()
    var kubesealBinaryPath by kubesealBinaryPathField::text
    private val tableEditor = SealingKeyEditor()
    var sealingKeys: List<SealingKey>
        get() = ArrayList(tableEditor.model.items)
        set(v) = tableEditor.reset(v)

    init {
        kubesealBinaryPathField.addBrowseFolderListener(
            TextBrowseFolderListener(
                FileChooserDescriptor(
                    true,
                    false,
                    false,
                    false,
                    false,
                    false
                )
            )
        )

        myMainPanel = FormBuilder.createFormBuilder()
            .addLabeledComponent(JBLabel("Path to kubeseal binary: "), kubesealBinaryPathField, 1, false)
            .addLabeledComponent(JBLabel("Available signing keys: "), tableEditor.createComponent())
            .addComponentFillVertically(JPanel(), 0)
            .panel
    }
}