package de.jonaseckstein.kubeseal

fun encrypt(
    executable: String,
    cert: String,
    namespace: String,
    name: String,
    value: String
): String {
    val kubesealBinary = ProcessBuilder(
        executable, "--raw",
        "--namespace", namespace,
        "--name", name,
        "--cert", cert
    ).start()

    // this is default encoding, but this might be correct in this case. Im not 100% sure
    kubesealBinary.outputWriter().use {
        it.write(value)
    }
    // TODO: handle status codes and other issues

    return String(kubesealBinary.inputStream.readAllBytes())
}
