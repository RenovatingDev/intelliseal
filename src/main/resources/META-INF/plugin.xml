<!-- Plugin Configuration File. Read more: https://plugins.jetbrains.com/docs/intellij/plugin-configuration-file.html -->
<idea-plugin>
    <!-- Unique identifier of the plugin. It should be FQN. It cannot be changed between the plugin versions. -->
    <id>de.jonaseckstein.KubeSeal</id>

    <!-- Public plugin name should be written in Title Case.
         Guidelines: https://plugins.jetbrains.com/docs/marketplace/plugin-overview-page.html#plugin-name -->
    <name>Sealify: KubeSeal Helper</name>

    <!-- A displayed Vendor name or Organization ID displayed on the Plugins Page. -->
    <vendor email="jonas.eckstein@web.de" url="https://gitlab.com/RenovatingDev">Jonas Eckstein</vendor>

    <!-- Description of the plugin displayed on the Plugin Page and IDE Plugin Manager.
         Simple HTML elements (text formatting, paragraphs, and lists) can be added inside of <![CDATA[ ]]> tag.
         Guidelines: https://plugins.jetbrains.com/docs/marketplace/plugin-overview-page.html#plugin-description -->
    <description><![CDATA[
    Adds an option to use <a href="https://github.com/bitnami-labs/sealed-secrets">kubeseal</a> inside of IntelliJ for YAML files.
    <ul>
     <li>Convert YAML files to sealed secrets with IntelliJ intentions (alt + enter).</li>
    </ul>
  ]]></description>

    <!-- Product and plugin compatibility requirements.
         Read more: https://plugins.jetbrains.com/docs/intellij/plugin-compatibility.html -->
    <depends>com.intellij.modules.platform</depends>
    <depends>com.intellij.modules.lang</depends>
    <depends>org.jetbrains.plugins.yaml</depends>

    <!-- Extension points defined by the plugin.
         Read more: https://plugins.jetbrains.com/docs/intellij/plugin-extension-points.html -->
    <extensions defaultExtensionNs="com.intellij">
        <intentionAction>
            <language>yaml</language>
            <className>de.jonaseckstein.kubeseal.intentions.KubesealIntention</className>
            <category>YAML</category>
        </intentionAction>
        <intentionAction>
            <language>yaml</language>
            <className>de.jonaseckstein.kubeseal.intentions.KubesealLastUsedKeyIntention</className>
            <category>YAML</category>
        </intentionAction>
        <intentionAction>
            <language>yaml</language>
            <className>de.jonaseckstein.kubeseal.intentions.KubesealAllChildrenIntention</className>
            <category>YAML</category>
        </intentionAction>
        <intentionAction>
            <language>yaml</language>
            <className>de.jonaseckstein.kubeseal.intentions.KubesealAllChildrenLastUsedKeyIntention</className>
            <category>YAML</category>
        </intentionAction>
        <applicationConfigurable
                parentId="tools"
                instance="de.jonaseckstein.kubeseal.settings.Configuration"
                id="de.jonaseckstein.KubeSeal"
                displayName="Sealify"/>
        <applicationService serviceImplementation="de.jonaseckstein.kubeseal.settings.ConfigurationState"/>
    </extensions>
    <change-notes>
        <![CDATA[
            <h3>0.3.0</h3>
            <ul>
                <li><b>feat:</b> Support for sealing multiple values at once by running the action on a parent element</li>
                <li><b>fix:</b> Sealing quoted scalars no longer includes the quote symbols in the sealed value</li>
            </ul>
            <h3>0.2.4</h3>
            <ul>
                <li>Support for IntelliJ version 2024.2</li>
            </ul>
            <h3>0.2.2</h3>
            <ul>
                <li>Support for IntelliJ version 2024.1</li>
            </ul>
            <h3>0.2.1</h3>
            <ul>
                <li>Support for IntelliJ version 2023.3.2 and later</li>
            </ul>
            <h3>0.2.0</h3>
            <ul>
                <li>Support for multiple sealing keys</li>
            </ul>
            <h3>0.1.0</h3>
            <ul>
                <li>Initial (beta) release</li>
            </ul>
    ]]>
    </change-notes>
</idea-plugin>
